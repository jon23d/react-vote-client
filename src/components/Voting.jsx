import React from 'react';
import PureRenderMixin from 'react-addons-pure-render-mixin';
import {connect} from 'react-redux';
import Winner from './Winner';
import Vote from './Vote';
import * as actionCreators from '../action_creators';

export const Voting = React.createClass({
    mixins: [PureRenderMixin],
    
    render: function() {
        return <div>
            {this.props.winner ?
                <Winner ref="winner" winner={this.props.winner} /> :
                <Vote {...this.props} />  // This 'spreads' props to the Vote component
                                          // https://facebook.github.io/react/docs/jsx-spread.html
            }
        </div>;
    }
});

function mapStateToProps(state) {
    return {
        pair: state.getIn(['vote', 'pair']),
        winner: state.get('winner'),
        hasVoted: state.get('hasVoted')
    }
}

connect(mapStateToProps)(Voting);

export const VotingContainer = connect(
    mapStateToProps,
    actionCreators // This somehow creates a vote prop on the Voting component linked to an action in
                   // action_creators.js.  My question, is how does it know to name it vote? Does it just
                   // call whatever prop is missing in actionCreators???
)(Voting);